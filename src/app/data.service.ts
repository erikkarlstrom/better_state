import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private stream = new BehaviorSubject<string>('string');
  public stream$ = this.stream.asObservable();

  constructor() { }

  getCurrentValue(): string {
    return this.stream.getValue();
  }

  setNewData(s: string) {
    this.stream.next(s);
  }
}
