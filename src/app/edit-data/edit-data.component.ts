import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-edit-data',
  templateUrl: './edit-data.component.html',
  styleUrls: ['./edit-data.component.css']
})
export class EditDataComponent implements OnInit {
  data: string;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.stream$.subscribe(data => this.data = data);
  }

  save() {
    this.dataService.setNewData(this.data);
  }

  cancel() {
    this.data = this.dataService.getCurrentValue();
  }
}
