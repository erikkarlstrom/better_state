import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-display-data',
  templateUrl: './display-data.component.html',
  styleUrls: ['./display-data.component.css']
})
export class DisplayDataComponent implements OnInit {

  data: string
  constructor(public dataService: DataService) {
  }
  ngOnInit(): void {
    this.dataService.stream$.subscribe(data => this.data = data);
  }


}
