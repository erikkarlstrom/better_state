## uppgift 1 diskutera

- hur funkar cancel

## uppgift 2 ändra data typ

- ändra data från en sträng till ett object som ser ut som:

{
    value : string,
    changed : Date
}

## uppgift 3 alternativa lösningar

- finns det fler sätt att lösa uppgift 2?

## uppgift 4 rxjs

- vad är skillnaden mellan rxjs subject och behaviour-subject och när kommer skillnaden att märkas?


